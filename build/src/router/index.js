import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import home from '@/components/home'
import register from '@/components/register'
import login from '@/components/login'
import staition from '@/components/staition'
import update_user from '@/components/update_user'
import a from '@/components/a'
import worker_order from '@/components/worker_order'
Vue.use(Router)

var routes = [
        {
          path:'/',
          name:'index',
          component:index
        },
        {
          path:'/home',
          name:'home',
          component:home
        },
         {
          path:'/register',
          name:'register',
          component:register
        },
      {
          path:'/login',
          name:'login',
          component:login
        },
         {
          path:'/staition',
          name:'staition',
          component:staition
        },
        {
          path:'/update_user',
          name:'update_user',
          component:update_user
        },
        {
          path:'/a',
          name:'a',
          component:a
        },
        {
          path:'/worker_order',
          name:'worker_order',
          component:worker_order
        },
]

export default new Router({
  routes:routes,
  mode:'history'   /*hash*/
})
