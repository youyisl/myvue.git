// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/*引入axios*/
import Axios from 'axios'
Vue.prototype.axios = Axios;

//Axios.defaults.withCredentials = true;
// Axios.defaults.baseURL='http://127.0.0.1:8000/'
import QS from 'qs'
Vue.prototype.qs = QS;

import ant from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

import setting from '@/js/setting.js'
Vue.use(ant)

Vue.prototype.$setting =setting;

// import VueSocketio from 'vue-socket.io'
// Vue.use(VueSocketio,'http://127.0.0.1:5000');

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  render: h => h(App)
})
