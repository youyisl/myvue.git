import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import home from '@/components/home'
import register from '@/components/register'
import login from '@/components/login'
import staition from '@/components/staition'
import update_user from '@/components/update_user'
import a from '@/components/a'
import worker_order from '@/components/worker_order'
import add_worker from '@/components/add_worker'
import task from '@/components/task'
import leftmenu from '@/components/leftmenu'
import Approval_status from '@/components/Approval_status'
import user_home from '@/components/user_home'
import message from '@/components/message'
Vue.use(Router)

var routes = [
        {
          path:'/',
          name:'index',
          component:index
        },
        {
          path:'/home',
          name:'home',
          component:home
        },
         {
          path:'/register',
          name:'register',
          component:register
        },
      {
          path:'/login',
          name:'login',
          component:login
        },
         {
          path:'/staition',
          name:'staition',
          component:staition
        },
        {
          path:'/update_user',
          name:'update_user',
          component:update_user
        },
        {
          path:'/a',
          name:'a',
          component:a
        },
        {
          path:'/worker_order',
          name:'worker_order',
          component:worker_order
        },
        {
          path:'/add_worker',
          name:'add_worker',
          component:add_worker
        },
        {
          path:'/task',
          name:'task',
          component:task
        },
        {
          path:'/leftmenu',
          name:'leftmenu',
          component:leftmenu
        },
        {
          path:'/Approval_status',
          name:'Approval_status',
          component:Approval_status
        },
        {
          path:'/user_home',
          name:'user_home',
          component:user_home
        },
        {
          path:'/message',
          name:'message',
          component:message
        },
]

export default new Router({
  routes:routes,
  mode:'history'   /*hash*/
})
